const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');

// Imports to manage user sessions
const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');
const { initialize } = require('./server/conf/passportSetup');

// Use express layout to manage ejs files
const expressLayouts = require('express-ejs-layouts');

// Environment variables
const env = require("dotenv").config({ path: "./.env" });
const sessionParams = require('./server/conf/expressSession');


// Development library to see objects content
const c = require('./server/conf/colors');

// Database & Models
const mongoose = require('mongoose');


// Map routes
const mainRoute = require('./server/routes/mainRoute');

// Run the server and initialize sessions
initialize(passport);
const app = express();

// Set socket server
const http = require('http').Server(app);
const io = require('socket.io')(http);

// Connect to MongoDB
mongoose.connect(process.env.DB_PASSWORD, { useNewUrlParser: true, useUnifiedTopology: true})
.then(() => console.log(c.iOrange + "[+] Listening database on cluster" + c.reset))
.catch(err => console.log(c.fRed + err + c.reset));


// Set the port in environment file for development
// If the environment file is missing start the server
// on port 3000 by default
const PORT = process.env.PORT || 3001;

// Used to parse the response for data extraction
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// View Engine Setup 
app.set('views', path.join(__dirname, 'public/views'));
app.set('view engine', 'ejs');
// Redirect on layout by default
app.use(expressLayouts);


// Use Express session properties
app.use(session(sessionParams.SESSION));

// Use passport session
app.use(passport.initialize());
app.use(passport.session());

// Connect flash middleware that returns
// flash messages during registration / connection
// Also used for layout view
app.use(flash());

// Global variables
app.use(function(req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg   = req.flash('error_msg');
    res.locals.error       = req.flash('error');
    next();
});


// Set Public folder /!\ Don't forget the first '/'
app.use('/public', express.static(path.join(__dirname, 'public')));

// Make reference to application routes
app.use('/', mainRoute);


// Listen for socket
io.on('connection', (socket) => {
    socket.on('chat message', msg => {
      io.emit('chat message', msg);
    });
  });
  

// Listen for connections on the server
http.listen(PORT, () => console.log(c.iOrange + `[+] Server listening on port ${PORT} !` + c.reset));