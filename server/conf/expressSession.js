const { Cookie } = require("express-session");


/*
* #########################################################
* ## SESSION LIFETIMES                                   ##
* #########################################################
* Define  session  lifetimes  used  in  COOKIE_MAX_AGE  and
* COOKIE_EXPIRE
*/
const TWO_HOURS = 1000 * 60 * 60 * 2;
/*
* #########################################################
* ## SESSION PARAMETERS                                  ##
* #########################################################
* The name of the session ID cookie to set in the response 
* (and  read  from  in  the request). The default value is 
* 'connect.sid'.
*/
const SESSION_NAME = 'sid';
/*
* Trust  the reverse proxy when setting secure cookies (via
* the  "X-Forwarded-Proto"  header).  The  default value is 
* undefined.
*
* 'true'  The  "X-Forwarded-Proto"  header  will  be  used.
* 'false'  All  headers  are  ignored and the connection is 
* considered  secure  only  if  there  is  a direct TLS/SSL 
* connection.
* 'undefined'  Uses  the "trust proxy" setting from express
*/
const SESSION_PROXY = undefined;
/*
* Forces the session to be saved back to the session store, 
* even  if  the  session  was  never  modified  during  the 
* request.  Depending  on your store this may be necessary, 
* but  it  can  also  create race conditions where a client 
* makes  two  parallel  requests to your server and changes 
* made  to  the  session in one request may get overwritten 
* when  the  other request ends, even if it made no changes 
* (this  behavior also depends on what store you're using).
*/
const SESSION_RESAVE = false;
/*
* Force  the  session  identifier cookie to be set on every 
* response. The expiration is reset to the original maxAge,
* resetting the expiration countdown.
* The default value is false.
* 
* With  this  enabled,  the  session identifier cookie will 
* expire in maxAge since the last response was sent instead
* of  in  maxAge since the session was last modified by the 
* server.
*
* This   is   typically  used  in  conjuction  with  short, 
* non-session-length  maxAge  values  to  provide  a  quick 
* timeout  of  the session data with reduced potentional of 
* it   occurring   during  on  going  server  interactions.
*/
const SESSION_ROLLING = true;
/*
* Forces  a  session that is "uninitialized" to be saved to 
* the  store. A session is uninitialized when it is new but
* not  modified.  Choosing false is useful for implementing 
* login   sessions,   reducing  server  storage  usage,  or 
* complying   with  laws  that  require  permission  before 
* setting a cookie. Choosing false will also help with race
* conditions   where   a  client  makes  multiple  parallel 
* requests without a session.
*
* The default value is true, but using the default has been
* deprecated,  as  the  default  will change in the future.
* Please  research  into  this  setting  and choose what is 
* appropriate to your use-case.
*
* Note  if  you  are  using  Session  in  conjunction  with 
* PassportJS, Passport will add an empty Passport object to
* the  session for use after a user is authenticated, which 
* will  be  treated  as  a  modification  to  the  session, 
* causing   it   to   be  saved.  This  has been  fixed  in 
* PassportJS 0.3.0
*/
const SESSION_SAVE_UNINITIALIZED = true;
/*
* This  is  the  secret used to sign the session ID cookie. 
* This  can  be  either a string for a single secret, or an
* array  of  multiple  secrets.  If  an array of secrets is 
* provided, only the first element will be used to sign the
* session  ID  cookie,  while  all  the  elements  will  be 
* considered  when  verifying  the  signature  in requests.
*/
const SESSION_SECRET = 'Zdzioj,nevZdfzjd,jez54edzqdcihZDZqs4f5zEfé-54F1E4ZR3VR_4Y8U9JDZDF24ef65sefc< hU';
/*
* The session store instance, defaults to a new MemoryStore 
* instance.
*/
const SESSION_STORE = undefined;
/*
* Control  the  result  of  unsetting  req.session (through
* delete, setting to null, etc.).
*
* The default value is 'keep'.
*
* 'destroy'  The  session  will be destroyed (deleted) when 
* the response ends.
* 'keep'  The  session  in  the  store  will  be  kept, but 
* modifications made during the request are ignored and not
* saved.
*/
const SESSION_UNSET = 'keep';
/*
* #########################################################
* ## COOKIE PARAMETERS                                   ##
* #########################################################
* Specifies the value for the Domain Set-Cookie  attribute. 
* By default,  no  domain  is  set,  and  most clients will
* consider the cookie to apply to only the  current domain.
*/
const COOKIE_DOMAIN = '';
/*
* Specifies the Date object to be the value for the Expires
* Set-Cookie  attribute.  By default, no expiration is set, 
* and  most  clients  will  consider this a "non-persistent 
* cookie"  and will delete it on a condition like exiting a 
* web browser application.
*
* Note  If  both expires and maxAge are set in the options, 
* then  the last one defined in the object is what is used.
*
* Note  The  expires  option  should  not  be set directly; 
* instead only use the maxAge option.
*/
const COOKIE_EXPIRE = new Date(Date.now() + TWO_HOURS);
/*
* Specifies  the  boolean value for the HttpOnly Set-Cookie 
* attribute.  When  truthy,  the HttpOnly attribute is set, 
* otherwise  it  is not. By default, the HttpOnly attribute 
* is set.
*
* Note  be  careful when setting this to true, as compliant 
* clients  will not allow client-side JavaScript to see the 
* cookie in document.cookie.
*/
const COOKIE_HTTP_ONLY = false;
/*
* Specifies  the  number  ( in milliseconds )  to  use when 
* calculating  the  Expires  Set-Cookie  attribute. This is  
* done  by taking the current server time and adding maxAge 
* milliseconds   to  the  value  to  calculate  an  Expires 
* datetime. By default, no maximum age is set.
*
* Note  If  both expires and maxAge are set in the options,  
* then  the last one defined in the object is what is used.
*/
const COOKIE_MAX_AGE = TWO_HOURS;
/*
* Specifies  the value for the Path Set-Cookie. By default, 
* this is set to '/', which is the root path of the domain.
*/
const COOKIE_PATH = "/";
/*
* Specifies  the  boolean or string to be the value for the 
* SameSite Set-Cookie attribute.
*
* 'true'   will  set  the  SameSite  attribute  to  Strict.
* 'false'    will   not   set   the   SameSite   attribute.
* 'lax' for lax same site enforcement.
* 'none' for an explicit cross-site cookie.
* 'strict' for strict same site enforcement.
*
* Note  This  is  an  attribute that has not yet been fully 
* standardized,  and  may  change  in the future. This also 
* means  many  clients  may  ignore  this  attribute  until 
* they understand it.
*/
const COOKIE_SAME_SITE = true;
/*
* Specifies  the  boolean  value  for the Secure Set-Cookie 
* attribute.  When  truthy,  the  Secure  attribute is set, 
* otherwise  it is not. By default, the Secure attribute is 
* not set.
*
* Note  be  careful when setting this to true, as compliant 
* clients  will  not  send the cookie back to the server in 
* the  future  if  the  browser  does  not  have  an  HTTPS 
* connection.
*
* Please  note  that  secure: true is a recommended option.
* However,  it  requires  an  https-enabled  website, i.e., 
* HTTPS  is necessary for secure cookies. If secure is set, 
* and  you  access your site over HTTP, the cookie will not 
* be  set.  If you have your node.js behind a proxy and are
* using  secure: true, you  need  to set "trust proxy" in
* express
*/
const COOKIE_SECURE = false;
/*
* #########################################################
* ## SESSION OBJECT                                      ##
* #########################################################
* Define session parameters before return.
*/
const SESSION = {
    name  : SESSION_NAME,
    cookie: { 
        domain  : COOKIE_DOMAIN,
        httpOnly: COOKIE_HTTP_ONLY,
        maxAge  : COOKIE_MAX_AGE,
        path    : COOKIE_PATH,
        sameSite: COOKIE_SAME_SITE,
        secure  : COOKIE_SECURE
    },
    proxy            : SESSION_PROXY,
    resave           : SESSION_RESAVE,
    rolling          : SESSION_ROLLING,
    saveUninitialized: SESSION_SAVE_UNINITIALIZED,
    secret           : SESSION_SECRET,
    store            : SESSION_STORE,
    unset            : SESSION_UNSET
};

module.exports = Object.freeze({
    SESSION: SESSION
});