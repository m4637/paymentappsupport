const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const UserSchema = require('../models/UserModel');

// Load User model
const User = require('../models/UserModel');

const c = require('../conf/colors');


module.exports = {
    ensureAuthenticated: function(req, res, next) {
      if (req.isAuthenticated()) {
        return next();
      }
      req.flash('error_msg', 'Please log in to view that resource');
      res.redirect('/login');
    },
    forwardAuthenticated: function(req, res, next) {
      if (!req.isAuthenticated()) {
        return next();
      }
      res.redirect('/dashboard');      
    },
    initialize: function(passport) {
        passport.use(
            // Use Local strategy options, is possible to be google, facebook, etc.
            new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
                // Check if there is a user matching
                UserSchema.findOne({ email: email }).then(user => {
                    if (!user) {
                        console.log(c.fYellow + '[-] That email is not registered' + c.reset);
                        // Send flash message to the front
                        return done(null, false, { message: 'That email is not registered' });
                    }
    
                    // Check if user email match with the password hash
                    bcrypt.compare(password + process.env.PEPPER, user.password, (err, isMatch) => {
                        if (err) throw err;
                        if (isMatch) {
                            console.log(c.info + '[i] Password match user' + c.reset);
                            return done(null, user);
                        } else {
                            console.log(c.fYellow + '[-] Password didn\'t match user' + c.reset);
                            return done(null, false, { message: 'Password incorrect' });
                        }
                    });
                });
            })
        );
    
        passport.serializeUser( function(user, done) {
            done(null, user.id);
        });
    
        passport.deserializeUser( function(id, done) {
            User.findById(id, function(err, user) {
            done(err, user);
            });
        });
    }
  };
  