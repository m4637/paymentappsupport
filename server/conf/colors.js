module.exports = {
    reset     : "\x1b[0m",
    bright    : "\x1b[1m",
    dim       : "\x1b[2m",
    underscore: "\x1b[4m",
    blink     : "\x1b[5m",
    reverse   : "\x1b[7m",
    hidden    : "\x1b[8m", 
    // ESC[ 38;5; (256 color code)m AND ESC[ Bright => Bold
    // 256 color code table : https://en.wikipedia.org/wiki/ANSI_escape_code
    iPurple: "\x1b[38;5;177m\x1b[1m",
    iOrange: "\x1b[38;2;252;127;0m\x1b[1m",
    // ESC[ 38;2;⟨r⟩;⟨g⟩;⟨b⟩ AND ESC[Bright => Bold
    info : "\x1b[38;2;41;184;219m\x1b[1m",

    fBlack  : "\x1b[30m",
    fRed    : "\x1b[31m",
    fGreen  : "\x1b[32m",
    fYellow : "\x1b[33m",
    fBlue   : "\x1b[34m",
    fMagenta: "\x1b[35m",
    fCyan   : "\x1b[36m",
    fWhite  : "\x1b[37m",

    bBlack  : "\x1b[40m",
    bRed    : "\x1b[41m",
    bGreen  : "\x1b[42m",
    bYellow : "\x1b[43m",
    bBlue   : "\x1b[44m",
    bMagenta: "\x1b[45m",
    bCyan   : "\x1b[46m",
    bWhite  : "\x1b[47m",

};