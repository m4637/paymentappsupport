const express = require('express');
const router = express.Router();


// Listen for connection on '/' and return some data
// for display and logical use
router.get('/', function(req, res){ 
    res.render('Home', {
        navtype: 'homepage',
    });
});

// Redirect to the FAQ application
router.get('/home', function(req, res) {
    res.status(200).redirect('http://localhost:5000/');
});


module.exports = router;